# README

## Gitflow

We're using [Gitflow](https://nvie.com/posts/a-successful-git-branching-model/) as the branching model.

## Workflow

### Branch naming guidelines

Branch names follow the default naming scheme, apart from releases and hotfixes.

- [Main branches](https://nvie.com/posts/a-successful-git-branching-model/#the-main-branches)
  - `master`
  - `develop`
- [Support branches](https://nvie.com/posts/a-successful-git-branching-model/#supporting-branches)
  - [Feature branches](https://nvie.com/posts/a-successful-git-branching-model/#feature-branches)
  - [Release branches](https://nvie.com/posts/a-successful-git-branching-model/#release-branches)
    - Naming convention changed from default.
    - Release branches **must** be named as the version it represents, i.e. `release/1.2.0` or `release/2.0.0`.
    - This is changed so the `master` branch is tagged correct when finishing the release.
  - [Hotfix branches](https://nvie.com/posts/a-successful-git-branching-model/#hotfix-branches)
    - Naming convention changed from default.
    - Hotfix branches **must** be named as the version it represents based on the current version, i.e. `hotfix/1.2.1` for release `1.2.0`.
    - This is changed so the `master` branch is tagged correct when finishing the hotfix.

### How to...

The [Gitflow](https://nvie.com/posts/a-successful-git-branching-model/) article explains how the different branches are started and finished. Our development tools have support for these steps.

The sections below outline how to do it in our project.

As a rule of thumb, all support branches (`feature`, `release`,  and `hotfix`) should be preceded by a pull-request before being finished.

#### Develop a new Feature

1. Start a new feature
2. Write the code
3. Push to Bitbucket
4. Create a pull request to `develop`
5. Wait for the pull request to be approved
6. Finish the feature
   1. This causes a local clean up of the feature branch
7. The feature branch is merged to `develop` and then deleted

#### Handle a Release

1. Start a new release
   1. Remember proper branch naming (version number)
2. Publish to staging
3. Fix any bugs
4. If necessary, create a pull request to `master`
   1. Wait for the pull request to be approved
   2. Finish the feature
      1. This causes a local clean up of the release branch.
5. If no pull request is made, finish the feature
6. The release branch is merged to `develop` and `master`. `master` is tagged with the release number
7. Release to production from `master`

#### Doing a Hotfix

1. Start a new hotfix
  1. Remember proper branch naming (hotfix number)
3. Fix the bugs
4. If necessary, create a pull request to `master`
   1. Wait for the pull request to be approved
   2. Finish the hotfix
      1. This causes a local clean up of the hotfix branch
5. If no pull request is made, finish the hotfix
6. The hotfix branch 

##### Note when finishing a hotfix

- If a release branch currently exists, the hotfix changes need to be merged into that release branch, instead of `develop`.
- Back-merging the hotfix into the release branch will eventually result in the bugfix being merged into `develop` too, when the release branch is finished.
- If work in `develop` immediately requires this bugfix and cannot wait for the release branch to be finished, you may safely merge the bugfix into `develop` now already as well.


#### Reviewing a pull request

1. Go through the changes
   1. It's up to the reviewer and developer how the review is conducted and who does corrections
2. Approve the pull request
3. **Do not** merge the pull request. It's up to the developer to finish the feature/release/hotfix
   1. If the pull request is merged, the local branch on the developer's PC isn't cleaned up
   2. No harm is done, though, if the pull request is merged. The feature is finished correct

#### Small changes (i.e. a typo or similar)

It is allowed to commit directly to `develop` without creating a feature branch and pull request.

## Tooling support

- Supported natively in [Sourcetree](https://www.sourcetreeapp.com/), [Git Extensions](https://gitextensions.github.io/), and [GitKraken](https://support.gitkraken.com/git-workflows-and-extensions/git-flow/)
- Available as extension for Visual Studio
  - [GitFlow for Visual Studio 2017](https://marketplace.visualstudio.com/items?itemName=vs-publisher-57624.GitFlowforVisualStudio2017)
  - [GitFlow for Visual Studio 2015](https://marketplace.visualstudio.com/items?itemName=vs-publisher-57624.GitFlowforVisualStudio2015)
- [Git command line extensions](https://github.com/petervanderdoes/gitflow-avh)
- [Bitbucket](https://bitbucket.org/blog/introducing-bitbucket-branching-model-support) supports the branching model in their web interface.

## Further reading

- For the best introduction to get started with git flow, please read Jeff Kreeftmeijer's blog post: https://jeffkreeftmeijer.com/git-flow/
- Gitflow cheatsheet: https://danielkummer.github.io/git-flow-cheatsheet/
