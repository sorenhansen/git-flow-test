﻿using System;
using System.Reflection;

namespace GitVersionConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{
			var assembly = Assembly.GetExecutingAssembly();
			var gitVersionInformationType = assembly.GetType("GitVersionInformation");
			var fields = gitVersionInformationType.GetFields();

			foreach (var field in fields)
			{
				Console.WriteLine($"{field.Name}: {field.GetValue(null)}");
			}

			// This is added in Feature-1
			// This is added in Feature-2
			// Hotfix
			// Another fix
			// This is added in Feature-3
			// Feature created in Git Extensions
			// Feature 123
			// Hotfix
			// This is added in Feature-3
		}
	}
}
